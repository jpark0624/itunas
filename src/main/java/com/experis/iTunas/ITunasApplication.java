package com.experis.iTunas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITunasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ITunasApplication.class, args);
	}

}
